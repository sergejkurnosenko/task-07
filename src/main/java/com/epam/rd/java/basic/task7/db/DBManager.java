package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.*;
import java.util.function.Function;


import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance;
    private Connection connection;
    private static final String APP_PROPS_FILE = "app.properties";

    public static synchronized DBManager getInstance() {
        return new DBManager();
    }

    private DBManager() {
        try {
            Properties prop = new Properties();
            InputStream input = new FileInputStream("app.properties");
            prop.load(input);
            connection = DriverManager.getConnection(prop.getProperty("connection.url"));
        } catch (SQLException | IOException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            String sql = "SELECT * FROM users";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                User user = User.createUser(resultSet.getString("login"));
                users.add(user);
            }
        } catch (SQLException throwables) {
            throw new DBException("error in findAllUsers()", throwables);
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        try {
            String log = user.getLogin();
            PreparedStatement statement = connection.prepareStatement("INSERT INTO users VALUES (DEFAULT, ?)");
            statement.setString(1, log);
            statement.execute();
        } catch (SQLException throwables) {
            throw new DBException("error in insertUser()", throwables);
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        try {
            for (User user :
                    users) {
                String log = user.getLogin();
                PreparedStatement statement = connection.prepareStatement("delete from users where login = ?");
                statement.setString(1, log);
                statement.execute();
            }
        } catch (SQLException throwables) {
            throw new DBException("error in deleteUsers()", throwables);
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        User user = new User();
        try {
            PreparedStatement statement = connection.prepareStatement("select id, login from users " +
                    "where login = ?");
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()) {
                user = User.createUser(resultSet.getString("login"));
                user.setId(resultSet.getInt("id"));
            }

        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
            throw new DBException("error in getUser()", throwables);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = new Team();
        try {
            PreparedStatement statement = connection.prepareStatement("select id, name from teams where name = ?");
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()) {
                team = Team.createTeam(resultSet.getString("name"));
                team.setId(resultSet.getInt("id"));
            }
        } catch (SQLException throwables) {
            throw new DBException("error in getTeam()", throwables);
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            String sql = "SELECT * FROM teams";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Team team = Team.createTeam(resultSet.getString("name"));
                teams.add(team);
            }
        } catch (SQLException throwables) {
            throw new DBException("error in findAllTeams()", throwables);
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        try {
            String name = team.getName();
            PreparedStatement statement = connection.prepareStatement("INSERT INTO teams VALUES (DEFAULT, ?)");
            statement.setString(1, name);
            statement.execute();
        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
            throw new DBException("error in insertTeam()", throwables);
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        try {
            for (Team team :
                    teams) {
                PreparedStatement statement = connection.prepareStatement(
                        "SELECT * FROM users_teams WHERE user_id = (SELECT id FROM users WHERE login = ?) and " +
                                "team_id = (SELECT id FROM teams WHERE name = ?)");
                statement.setString(1, user.getLogin());
                statement.setString(2, team.getName());
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()){
                    throw new DBException("error in setTeamsForUser()", new Throwable());
                }
            }

            for (Team team :
                    teams) {
                PreparedStatement statement = connection.prepareStatement(
                        "INSERT INTO users_teams VALUES ((SELECT id FROM users WHERE login = ?), " +
                                "(SELECT id FROM teams WHERE name = ?))");
                statement.setString(1, user.getLogin());
                statement.setString(2, team.getName());
                statement.execute();
            }
        } catch (SQLException throwables) {
            throw new DBException("error in setTeamsForUser()", throwables);
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT t.name, team_id FROM users_teams JOIN " +
                    "teams t on t.id = users_teams.team_id where user_id = (SELECT id FROM users WHERE login = ?)");
            statement.setString(1,user.getLogin());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Team team = Team.createTeam(resultSet.getString("name"));
                team.setId(resultSet.getInt("team_id"));
                teams.add(team);
            }
        } catch (SQLException throwables) {
            throw new DBException("error in getUserTeams()", throwables);
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try {

            String name = team.getName();
            PreparedStatement statement = connection.prepareStatement("delete from teams where name = ?");
            statement.setString(1, name);
            statement.execute();

        } catch (SQLException throwables) {
            throw new DBException("error in deleteTeam()", throwables);
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        try {
            String name = team.getName();
            PreparedStatement statement = connection.prepareStatement("update teams set name = ? where id = " +
                    "(SELECT id FROM teams WHERE name = ?)");
            statement.setString(1, name);
            statement.setString(2, team.getOldName());
            statement.execute();
        } catch (SQLException throwables) {
            throw new DBException("error in updateTeam()", throwables);
        }
        return true;
    }
}
